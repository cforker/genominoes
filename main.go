package main

import (
	"flag"
	"fmt"
	"log"
	"math"
	"math/bits"
	"os"
	"path/filepath"
)

var (
	order  *int
	width  int
	height int
	valid  map[uint64]bool
)

func main() {

	order = flag.Int("order", 5, "Max area of the polyominoes to generate.")
	gen := flag.Bool("gen", false, "Generate 3d models.")
	dir := flag.String("dir", "", "Directory to write 3d model files.")
	flag.Parse()
	fmt.Println("Polyomino generation.")

	if *order > 8 {
		log.Fatal("Only valid for orders up to 8.")
	}
	width = *order
	height = int(math.Ceil(float64(*order) / 2.0))
	valid = make(map[uint64]bool)

	fmt.Printf("Grid size is %dx%d.\n", width, height)

	candidates, err := binaryCombinations(width*height, *order)
	if err != nil {
		log.Fatal(err)
	}

	for _, c := range candidates {
		if !validate(c) {
			continue
		}
		cnorm := shiftUp(shiftLeft(c))
		if valid[cnorm] {
			continue
		}
		// Check all 8 orientations for dupes.
		canonical := cnorm
		minimal := cnorm
		dupe := false
		v := true
		for i := 0; i < 2; i++ {
			for j := 0; j < 4; j++ {
				if valid[cnorm] && v {
					dupe = true
				}
				v, cnorm = rotateValid(cnorm)
				// prefer a rotation where bit 0 is populated.
				if v && (cnorm&1) == 1 {
					if (canonical&1) == 0 || cnorm < canonical {
						canonical = cnorm
					}
				}
				if v && cnorm < minimal {
					minimal = cnorm
				}
			}
			cnorm = flip(cnorm)
		}
		if !dupe {
			valid[canonical] = true
		}
	}

	fmt.Printf("Found %d free polyominoes.\n", len(valid))
	if *gen {
		fmt.Printf("Generating 3d models.\n")
	}
	for k := range valid {
		fmt.Printf("Valid piece 0x%X:\n", k)
		if *gen {
			write3dModel(k, *dir)
		}
		debugPrint(k)
	}

}

func shiftLeft(mask uint64) uint64 {
	for {
		for i := uint(0); i < uint(width); i++ {
			if (mask>>(i*uint(width)))&1 == 1 {
				return mask
			}
		}
		mask = mask >> 1
	}
}

func shiftUp(mask uint64) uint64 {
	for {
		if mask&((1<<uint(width))-1) != 0 {
			return mask
		}
		mask = mask >> uint(width)
	}
}

// This doesn't work for larger orders than 8 because it overflows the
// uint64 when rotating.
func rotateValid(mask uint64) (bool, uint64) {
	var rot uint64 = 0
	for y := 0; y < width; y++ {
		for x := 0; x < width; x++ {
			rot += (mask & 1) << uint(width-(y+1)+width*x)
			mask = mask >> 1
		}
	}
	rot = shiftLeft(rot)
	rot = shiftUp(rot)
	// If the rotated number doesn't fit in the grid it is invalid.
	return rot < (1 << uint(width*height)), rot
}

func flip(mask uint64) uint64 {
	var f uint64 = 0
	for y := 0; y < width; y++ {
		for x := 0; x < width; x++ {
			f += (mask & 1) << uint(width-1-x+width*y)
			mask = mask >> 1
		}
	}
	f = shiftLeft(f)
	f = shiftUp(f)
	return f
}

// Returns all the size n integers with r ones.
func binaryCombinations(n, r int) ([]uint64, error) {
	if n < 0 || r > n || r < 0 {
		return []uint64{}, fmt.Errorf("input values are bad: %d, %d", n, r)
	}
	result := make([]uint64, 0)
	fmt.Printf("Finding combinations for: %dc%d\n", n, r)
	for i := uint64(0x01); i < uint64(1<<uint(n)); i++ {
		if bits.OnesCount64(i) == r {
			result = append(result, i)
		}
	}
	return result, nil
}

// validate checks if the bit mask could be a valid polyomino by checking
// if all the populated 1's are connected.
func validate(mask uint64) bool {
	temp := mask
	q := make([]uint, 0)
	seen := make(map[uint]bool)
	// Find a starting 1.
	for i := uint(0); i < uint(width*height); i++ {
		if temp&0x1 == 1 {
			q = append(q, i)
			break
		}
		temp = temp >> 1
	}

	// BFS with a queue to determine if there are N connected ones.
	done := false
	for !done {
		if len(q) == 0 && len(seen) == *order {
			return true
		}
		if len(q) == 0 && len(seen) != *order {
			return false
		}
		if seen[q[0]] {
			q = q[1:]
			continue
		}
		// Get all the neighbors and enqueue the 1's for investigation.
		p := q[0]
		q = q[1:]
		seen[p] = true
		if p < uint((width*(height-1))) && (mask>>(p+uint(width)))&1 == 1 {
			q = append(q, p+uint(width))
		}
		if p >= uint(width) && (mask>>(p-uint(width)))&1 == 1 {
			q = append(q, p-uint(width))
		}
		if (p%uint(width)) != 0 && (mask>>(p-1))&1 == 1 {
			q = append(q, p-1)
		}
		if (p%uint(width)) != uint(width-1) && (mask>>(p+1))&1 == 1 {
			q = append(q, p+1)
		}

	}
	return false
}

func debugPrint(mask uint64) {
	for i := 0; i < height; i++ {
		for j := 0; j < width; j++ {
			fmt.Printf("%b", mask&0x1)
			mask = mask >> 1
		}
		fmt.Printf("\n")
	}
}

func write3dModel(mask uint64, dir string) {
	var err error
	if dir == "" {
		dir, err = os.Getwd()
		if err != nil {
			fmt.Printf("Error from os.Getwd: %v", err)
		}
	}
	fmt.Printf("Writing to dir: %s\n", dir)
	// If the specified directory doesn't have the .scad
	// dependency file then copy it there.
	dep_fname := "blokus_piece.scad"
	if _, err := os.Stat(filepath.Join(dir, dep_fname)); os.IsNotExist(err) {
		fd, err := os.Create(filepath.Join(dir, dep_fname))
		if err != nil {
			log.Fatal("Could not create the %s file in %s: %v", dep_fname, dir, err)
		}
		defer fd.Close()
		fmt.Fprintf(fd, "%s\n", blokus_scad_content)
	}
	fname := fmt.Sprintf("B%d_0x%X.scad", *order, mask)
	f, err := os.Create(filepath.Join(dir, fname))
	if err != nil {
		log.Fatal("Could not open file: %v", err)
	}
	defer f.Close()
	fmt.Fprintf(f, "use <%s>\n\nunion(){\n", dep_fname)
	for y := 0; y < height; y++ {
		for x := 0; x < width; x++ {
			if (mask & 1) == 1 {
				fmt.Fprintf(f, "    blokus_unit([%d, %d]);\n", x, y)
			}
			mask = mask >> 1
		}
	}
	fmt.Fprintf(f, "}\n")
	fmt.Printf("Wrote to %s\n", filepath.Join(dir, fname))
}
