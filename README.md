# Gen-ominoes #

Generate all possible free polyominoes.

### What is this repository for? ###

* Generate uint64 encodings for the free polyominoes of a given size (used for the board game blokus).
* Generate OpenSCAD 3d models of the generated pieces for 3d printing.
* Version 0.1

### How do I get set up? ###

* go build
* ./genominoes -order 5 -gen_models=true # generates the 21 pentominoes with 3d models in your pwd.

### Who do I talk to? ###

* cforker@gmail.com