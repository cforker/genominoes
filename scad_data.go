package main

var blokus_scad_content string = `
//single unit dimensions
middle_thickness        = 2; //this is the flat middle section that connects everything together
middle_length           = 14.75; //this is the length of 1 square of the middle section

outside_thickness       = 1; //these are the slightly tapered outer sections on both the top and the bottom
outside_base_length     = 13.6; //this is the length of the side of the outside square at the base (long) side of the taper
outside_top_length      = 13; //this is the length of the side of the outside square at the top (short) side of the taper

module blokus_base_unit(){
    translate([0,0,middle_thickness/2]) rotate([0,0,45]) cylinder(outside_thickness,d1=sqrt(2)*outside_base_length,d2=sqrt(2)*outside_top_length,$fn=4);
    cube([middle_length,middle_length,middle_thickness],center=true);
    translate([0,0,-1*middle_thickness/2]) rotate([180,0,45]) cylinder(outside_thickness,d1=sqrt(2)*outside_base_length,d2=sqrt(2)*outside_top_length,$fn=4);
}

//this module takes in a vector in blokus units
module blokus_unit(vector){
    translate([vector[0]*middle_length, vector[1]*middle_length, 0]) blokus_base_unit();
}
`
